# Teach Me How to Git Tutorial
In this directory you will find the slides from the in person tutorial. In this file I will include some links to things we talked about but aren't in the slides.

- [git documentation](https://git-scm.com/docs)
- [gitignore documentation](https://git-scm.com/docs/gitignore)
- [github default .gitignores](https://github.com/github/gitignore)
- [git book: working with remotes](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes)
- [GitHub explore page](https://github.com/explore) great place to find cool currated open source projects
- [Open Source Guides](https://opensource.guide/)
- [git cheatsheet](https://services.github.com/on-demand/resources/cheatsheets/)
- [atlassian guides: pull requests on bitbucket](https://www.atlassian.com/git/tutorials/making-a-pull-request)


